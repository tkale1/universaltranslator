//
//  ViewController.m
//  FGTranslatorDemo
//
//  Created by Tanmay Kale.
//  Copyright © 2017 Tanmay Kale. All rights reserved.
//


#import "ViewController.h"
#import "MainScreenViewController.h"
#import "FGTranslator.h"
#import "SVProgressHUD.h"

@interface ViewController ()

//@property (nonatomic, weak) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UITextField *textView;

@end


@implementation ViewController
//@synthesize theData;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //self.textView.text = @"Hello";
    //NSLog(@"VC 2 Language Selected :    %@",selectedValue);
    //self.textView.
    [FGTranslator flushCache];
    [FGTranslator flushCredentials];
}

- (FGTranslator *)translator {
   
    FGTranslator *translator;
    // using Google Translate
    translator = [[FGTranslator alloc] initWithGoogleAPIKey:@"AIzaSyAzc6MCYVFwNR8QtbnVe_9FYswH50tDB9M"];
    return translator;
}

- (NSLocale *)currentLocale {
    NSLocale *locale = [NSLocale currentLocale];

        #if TARGET_IPHONE_SIMULATOR
            // handling Apple bug
            // http://stackoverflow.com/a/26769277/211692
            return [NSLocale localeWithLocaleIdentifier:[locale localeIdentifier]];
        #else
            return locale;
        #endif
}

- (IBAction)translate:(UIButton *)sender
{
    [SVProgressHUD show];
    
    [self.textView resignFirstResponder];

    [self.translator translateText:self.textView.text
                   completion:^(NSError *error, NSString *translated, NSString *sourceLanguage)
    {
         if (error)
         {
             [self showErrorWithError:error];
             
             [SVProgressHUD dismiss];
         }
         else
         {
             NSString *fromLanguage = [[self currentLocale] displayNameForKey:NSLocaleIdentifier value:sourceLanguage];
             
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:fromLanguage ? [NSString stringWithFormat:@"from %@", fromLanguage] : nil
                                                             message:translated
                                                            delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
             [alert show];
            
             [SVProgressHUD dismiss];
         }
     }];
}

- (IBAction)detect:(UIButton *)sender
{
    [SVProgressHUD show];
    
    [self.textView resignFirstResponder];
    
    
    [self.translator detectLanguage:self.textView.text completion:^(NSError *error, NSString *detectedSource, float confidence)
    {
        if (error)
        {
            [self showErrorWithError:error];
            
            [SVProgressHUD dismiss];
        }
        else
        {
            NSString *fromLanguage = [[self currentLocale] displayNameForKey:NSLocaleIdentifier value:detectedSource];
            
            NSString *confidenceMessage = confidence == FGTranslatorUnknownConfidence
                ? @"unknown confidence"
                : [NSString stringWithFormat:@"%.1f%% sure", confidence * 100];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:fromLanguage
                                                            message:confidenceMessage
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            
            [SVProgressHUD dismiss];
        }
    }];
}

- (IBAction)supportedLanguages:(id)sender
{
    [SVProgressHUD show];
    
    [self.textView resignFirstResponder];
    
    [self.translator supportedLanguages:^(NSError *error, NSArray *languageCodes)
    {
        if (error)
        {
            [self showErrorWithError:error];
            
            [SVProgressHUD dismiss];
        }
        else
        {
            NSMutableString *languageMessage = [NSMutableString new];
            NSLocale *locale = [self currentLocale];
            for (NSString *code in languageCodes)
                [languageMessage appendFormat:@"%@\n", [locale displayNameForKey:NSLocaleIdentifier value:code]];
           
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%ld Supported Languages", (long)languageCodes.count]
                                                            message:languageMessage
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            
            [SVProgressHUD dismiss];
        }
    }];
}

- (void)showErrorWithError:(NSError *)error
{
    NSLog(@"FGTranslator failed with error: %@", error);
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                    message:error.localizedDescription
                                                   delegate:nil
                                          cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

@end
