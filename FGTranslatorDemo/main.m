//
//  main.m
//  FGTranslatorDemo
//
//
//  Created by Tanmay Kale.
//  Copyright © 2017 Tanmay Kale. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
