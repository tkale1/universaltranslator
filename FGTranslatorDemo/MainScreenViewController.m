//
//  MainScreenViewController.m
//  FGTranslatorDemo
//
//  Created by Tanmay Kale.
//  Copyright © 2017 Tanmay Kale. All rights reserved.
//


#import "MainScreenViewController.h"
#import "ViewController.h"

@interface MainScreenViewController () <UIPickerViewDataSource, UIPickerViewDelegate>

{
    NSArray *_pickerData;
}
@end

@implementation MainScreenViewController
//@synthesize selectedValue;

NSString *selectedValue;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Fetching the size(Height, Width) of the Screen
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    CGFloat screenHeight = screenSize.height;
    
    
//    [[UIImage imageNamed:@"Logo"] drawInRect:self.view.bounds];
//    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    
    _pickerData = @[@"English", @"Hindi", @"Spanish", @"Marathi", @"Arabic", @"Chinese",@"French", @"German",@"Urdu",@"Gujarati"];
    
    self.picker.dataSource = self;
    self.picker.delegate = self;
    [_picker setFrame: CGRectMake(screenWidth/2 - 55, screenHeight/2 - 55, 15, 15)];//(xPoint, 50.0f, pickerWidth, 200.0f)];
    
    }


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonClick:(id)sender {
    
    NSString *YourselectedTitle = [[NSString alloc] initWithString:[self->_pickerData objectAtIndex:[self.picker selectedRowInComponent:0]]];
    
    NSLog(@"Main VC Selected language =   %@", YourselectedTitle);
    if([YourselectedTitle  isEqual: @"English"])
    {
        selectedValue = @"en";
    }
    else if([YourselectedTitle  isEqual: @"Hindi"])
    {
        selectedValue = @"hi";
    }
    else if([YourselectedTitle  isEqual: @"Spanish"])
    {
        selectedValue = @"es";
    }
    else if([YourselectedTitle  isEqual: @"Marathi"])
    {
        selectedValue = @"mr";
    }
    else if([YourselectedTitle  isEqual: @"Hindi"])
    {
        selectedValue = @"hi";
    }
    else if([YourselectedTitle  isEqual: @"Arabic"])
    {
        selectedValue = @"ar";
    }
    else if([YourselectedTitle  isEqual: @"Chinese"])
    {
        selectedValue = @"zh-CN";
    }
    else if([YourselectedTitle  isEqual: @"French"])
    {
        selectedValue = @"fr";
    }
    else if([YourselectedTitle  isEqual: @"Gujarati"])
    {
        selectedValue = @"gu";
    }
    else if([YourselectedTitle  isEqual: @"German"])
    {
        selectedValue = @"de";
    }
    else if([YourselectedTitle  isEqual: @"Urdu"])
    {
        selectedValue = @"ur";
    }
}


// The number of columns of data
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
// The number of rows of data
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return _pickerData.count;
}
// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return _pickerData[row];
}



@end
