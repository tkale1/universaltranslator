//
//  MainScreenViewController.h
//  FGTranslatorDemo
//
//  Created by Tanmay Kale.
//  Copyright © 2017 Tanmay Kale. All rights reserved.
//


#import <UIKit/UIKit.h>

extern NSString *selectedValue;

@interface MainScreenViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIPickerView *picker;
//@property (weak, nonatomic) NSString *selectedValue;
@end
